﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour {

    [SerializeField] Vector3 moveOnVector3 = new Vector3(10f, 10f, 10f);
    [SerializeField] float period = 2f;

    [Range(0, 1)] float movementFactor;
    
    Vector3 startingPoint;

	void Start ()
    {
        startingPoint = transform.position;
	}

	void Update ()
    {
        if(period != 0)
        { 
        float cycles = Time.time / period;
        const float tau = Mathf.PI * 2; // about 6.28
        float rawSinWave = Mathf.Sin(cycles * tau); //goes -1 to 1
        movementFactor = rawSinWave / 2f + 0.5f; //goes 0 to 1
        Vector3 offset = movementFactor * moveOnVector3;
        transform.position = startingPoint + offset;
	    }
	}
}
