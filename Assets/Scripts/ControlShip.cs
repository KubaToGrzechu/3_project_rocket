﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlShip : MonoBehaviour
{
    
    Rigidbody rb;
    [SerializeField] float zAccess;
    [SerializeField] float timeFreeze = 2f;
    [SerializeField] float PowerUp;
    [SerializeField] float PowerDown;
    [SerializeField] enum State {Alive, Dying, Transcending}
    State state = State.Alive;

    bool disableColis = false;

    [SerializeField] AudioSource ad;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip explosionSound;
    [SerializeField] AudioClip NextLvlSound;
    [SerializeField] ParticleSystem mainEngineParticle;
    [SerializeField] ParticleSystem explosionParticle;
    [SerializeField] ParticleSystem NextLvlParticle;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }

    void Update()
    {
        WhenCanRotate();
        debugKyes();

    }

    private void WhenCanRotate()
    {
        if (state == State.Alive)
        {
            goUp();
            Rotate();
        }
    }

    private void debugKyes()
    {
        if (Debug.isDebugBuild)
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                LoadNextSceen();
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                disableColis = !disableColis;
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        

        if (state == State.Alive && !disableColis )
        {
                 switch (collision.gameObject.tag)
                {
                    case "Friendly":
                        break;
                    case "Finish":
                        state = State.Transcending;
                        ad.Stop();
                        ad.PlayOneShot(NextLvlSound);
                        NextLvlParticle.Play();
                    
                        Invoke("LoadNextSceen", timeFreeze);
                        break;
                    default:
                        state = State.Dying;
                        ad.Stop();
                        ad.PlayOneShot(explosionSound);
                        explosionParticle.Play();
                        Invoke("LoadDeath", timeFreeze);
                        break;
                }
       
        }
    }

    private void LoadDeath()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    private void LoadNextSceen()
    {
        int nextScene = SceneManager.GetActiveScene().buildIndex;

        if(nextScene != 2)
        {
            SceneManager.LoadScene(nextScene + 1);
        }else
        {
            SceneManager.LoadScene(0);
        }
    }

    void goUp()
    {
       
        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow))
        {
            rb.AddRelativeForce(PowerUp * Vector3.up * Time.deltaTime);

            if (!ad.isPlaying)
            {
                ad.PlayOneShot(mainEngine);
                mainEngineParticle.Play();
            }
        }
        else
        {
            ad.Stop();
            mainEngineParticle.Stop();
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            rb.AddRelativeForce(PowerDown * -Vector3.up * Time.deltaTime);
        }

    }

    private void Rotate()
    {
        rb.freezeRotation = true;
        float rs = zAccess * Time.deltaTime;
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.forward * rs);
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(-Vector3.forward * rs);
        }
        rb.freezeRotation = false;
    }
}
