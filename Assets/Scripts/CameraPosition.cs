﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : MonoBehaviour {

    public GameObject player;       


    public Vector3 offset;        

    
    void Start()
    {
        
       offset = transform.position - player.transform.position + new Vector3(4f,4f,4f);
    }

    
    void LateUpdate()
    {
        
        transform.position = (player.transform.position/8 + offset);
    }
}